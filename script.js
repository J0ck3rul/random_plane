const number = document.querySelector('#number');
const selectedValueElement = document.querySelector('#selectedValue');

const MAX_TIME = Math.floor(Math.random() * 25 + 10);
const INITIAL_VALUE = 1.01;

let selectedValue = null;
let iteration = 1;
let value = INITIAL_VALUE;

setInterval(() => {
  iteration++;
  if (iteration > MAX_TIME) {
    number.classList.add('number-overflow');
    return;
  }

  const random = Math.random() * 0.2 + 1;
  const sign = Math.round(Math.floor(Math.random() * 4 + 1)) > 3 ? 1 : -1;
  if (value < 15) {
    const newValue = value * (random * sign);
    value = (Math.round(newValue * 100) / 100).toFixed(2);
  }
  number.textContent = value;
}, 100);

document.addEventListener('keydown', (key) => {
  if (selectedValue) {
    return;
  }
  if (key.code === 'Space') {
    selectedValue = (Math.round(value * 100) / 100).toFixed(2);
    selectedValueElement.textContent = selectedValue;
  }
});
